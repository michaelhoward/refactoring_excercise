import java.util.Arrays;


/**
 * Created by stephen on 3/31/17.
 */
class ItemProcessor {
    private Item[] items;
    
    public ItemProcessor(Item[] items) {
        this.items = items;
    }
    
    /**
     * 
     - All items have a daysTillExpiry value
     - All items have a quality value
     - At the end of each day our system lowers both values for every item (excluding the exceptions below)
     
     - Once the daysTillExpiry reaches 0, quality decreases twice as fast
     - The quality of an item is never negative
     - "Wine" actually increases in quality the older it gets
     - The quality of an item is never more than 50
     - "Legendary Item" never has daysTillExpiry or quality decrease
     - "Concert tickets", like Wine, increases in quality as its daysTillExpiry value decreases;
     quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
     quality drops to 0 after the concert.
 
     We require the following update to the system:
 
     - A "Perishable Item" decreases in quality twice as fast as normal items
     * 
     */
    
    public void updateQuality() {
        Arrays.stream(items).forEach(Item::process);
    }
}
