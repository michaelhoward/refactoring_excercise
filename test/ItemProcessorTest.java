import org.junit.Assert;
import org.junit.Test;


/**
 * Created by stephen on 3/31/17.
 */
public class ItemProcessorTest {
    
    
    @Test
    public void testNormalItem() {
        Item[] items = new Item[] {
                new Item.NormalItem( 10, 20),
                new Item.NormalItem( 10, 49),
                new Item.NormalItem( 2, 49)};
        
        process(items);
        
        Assert.assertEquals(new Item.NormalItem(8, 18), items[0]);
        Assert.assertEquals(new Item.NormalItem(8, 47), items[1]);
        Assert.assertEquals(new Item.NormalItem(0, 46), items[2]);
    }
    
    @Test
    public void testWineItem() {
        Item[] items = new Item[] {
                new Item.WineItem( 2, 0),
                new Item.WineItem( 2, 49)};
    
        process(items);
        
        Assert.assertEquals(new Item.WineItem( 0, 2), items[0]);
        Assert.assertEquals(new Item.WineItem( 0, 50), items[1]);
    }
    
    @Test
    public void testLegendaryItem() {
        
        Item[] items = new Item[] {
                new Item.LegendaryItem( 0, 80),
                new Item.LegendaryItem( -1, 80) };
    
        process(items);
        
        Assert.assertEquals(new Item.LegendaryItem( 0, 80), items[0]);
        Assert.assertEquals(new Item.LegendaryItem( -1, 80), items[1]);
    }
    
    @Test
    public void testConcertItem() {
        
        Item[] items = new Item[] {
                new Item.ConcertTickets(15, 20),
                new Item.ConcertTickets( 10, 49),
                new Item.ConcertTickets( 1, 49) };
    
        process(items);
        
        Assert.assertEquals(new Item.ConcertTickets( 13, 22), items[0]);
        Assert.assertEquals(new Item.ConcertTickets( 8, 50), items[1]);
        Assert.assertEquals(new Item.ConcertTickets( 0, 0), items[2]);
    }
    
    @Test
    public void testPerishableItem() {
        Item[] items = new Item[] {
                new Item.PerishableItem( 3, 6),
                new Item.PerishableItem( 6, 22) };
    
        process(items);
    
    
        Assert.assertEquals(new Item.PerishableItem( 1, 2), items[0]);
        Assert.assertEquals(new Item.PerishableItem( 4, 18), items[1]);
    }
    
    private void process(Item[] items) {
        ItemProcessor app = new ItemProcessor(items);
        
        int days = 2;
        
        for (int i = 0; i < days; i++) {
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }
    
    @Test
    public void testGoldenMaster() {
        Item[] items = new Item[] {
                new Item.NormalItem(10, 20),
                new Item.WineItem( 2, 0),
                new Item.NormalItem(5, 7),
                new Item.LegendaryItem(0, 80),
                new Item.LegendaryItem( -1, 80),
                new Item.ConcertTickets(15, 20),
                new Item.ConcertTickets(10, 49),
                new Item.ConcertTickets(5, 49) };
        
        ItemProcessor app = new ItemProcessor(items);
        
        int days = 20;
        
        StringBuilder s = new StringBuilder();
        
        for (int i = 0; i < days; i++) {
            for (Item item : items) {
                s.append(item+"\n");
            }
            s.append("\n");
            app.updateQuality();
        }
        
        //System.out.println(s.toString());
        
        Assert.assertEquals("Item(daysTillExpiry=10, quality=20)\n"
                + "Item(daysTillExpiry=2, quality=0)\n"
                + "Item(daysTillExpiry=5, quality=7)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=15, quality=20)\n"
                + "Item(daysTillExpiry=10, quality=49)\n"
                + "Item(daysTillExpiry=5, quality=49)\n"
                + "\n"
                + "Item(daysTillExpiry=9, quality=19)\n"
                + "Item(daysTillExpiry=1, quality=1)\n"
                + "Item(daysTillExpiry=4, quality=6)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=14, quality=21)\n"
                + "Item(daysTillExpiry=9, quality=50)\n"
                + "Item(daysTillExpiry=4, quality=50)\n"
                + "\n"
                + "Item(daysTillExpiry=8, quality=18)\n"
                + "Item(daysTillExpiry=0, quality=2)\n"
                + "Item(daysTillExpiry=3, quality=5)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=13, quality=22)\n"
                + "Item(daysTillExpiry=8, quality=50)\n"
                + "Item(daysTillExpiry=3, quality=50)\n"
                + "\n"
                + "Item(daysTillExpiry=7, quality=17)\n"
                + "Item(daysTillExpiry=-1, quality=4)\n"
                + "Item(daysTillExpiry=2, quality=4)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=12, quality=23)\n"
                + "Item(daysTillExpiry=7, quality=50)\n"
                + "Item(daysTillExpiry=2, quality=50)\n"
                + "\n"
                + "Item(daysTillExpiry=6, quality=16)\n"
                + "Item(daysTillExpiry=-2, quality=6)\n"
                + "Item(daysTillExpiry=1, quality=3)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=11, quality=24)\n"
                + "Item(daysTillExpiry=6, quality=50)\n"
                + "Item(daysTillExpiry=1, quality=50)\n"
                + "\n"
                + "Item(daysTillExpiry=5, quality=15)\n"
                + "Item(daysTillExpiry=-3, quality=8)\n"
                + "Item(daysTillExpiry=0, quality=2)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=10, quality=25)\n"
                + "Item(daysTillExpiry=5, quality=50)\n"
                + "Item(daysTillExpiry=0, quality=50)\n"
                + "\n"
                + "Item(daysTillExpiry=4, quality=14)\n"
                + "Item(daysTillExpiry=-4, quality=10)\n"
                + "Item(daysTillExpiry=-1, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=9, quality=27)\n"
                + "Item(daysTillExpiry=4, quality=50)\n"
                + "Item(daysTillExpiry=-1, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=3, quality=13)\n"
                + "Item(daysTillExpiry=-5, quality=12)\n"
                + "Item(daysTillExpiry=-2, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=8, quality=29)\n"
                + "Item(daysTillExpiry=3, quality=50)\n"
                + "Item(daysTillExpiry=-2, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=2, quality=12)\n"
                + "Item(daysTillExpiry=-6, quality=14)\n"
                + "Item(daysTillExpiry=-3, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=7, quality=31)\n"
                + "Item(daysTillExpiry=2, quality=50)\n"
                + "Item(daysTillExpiry=-3, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=1, quality=11)\n"
                + "Item(daysTillExpiry=-7, quality=16)\n"
                + "Item(daysTillExpiry=-4, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=6, quality=33)\n"
                + "Item(daysTillExpiry=1, quality=50)\n"
                + "Item(daysTillExpiry=-4, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=0, quality=10)\n"
                + "Item(daysTillExpiry=-8, quality=18)\n"
                + "Item(daysTillExpiry=-5, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=5, quality=35)\n"
                + "Item(daysTillExpiry=0, quality=50)\n"
                + "Item(daysTillExpiry=-5, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-1, quality=8)\n"
                + "Item(daysTillExpiry=-9, quality=20)\n"
                + "Item(daysTillExpiry=-6, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=4, quality=38)\n"
                + "Item(daysTillExpiry=-1, quality=0)\n"
                + "Item(daysTillExpiry=-6, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-2, quality=6)\n"
                + "Item(daysTillExpiry=-10, quality=22)\n"
                + "Item(daysTillExpiry=-7, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=3, quality=41)\n"
                + "Item(daysTillExpiry=-2, quality=0)\n"
                + "Item(daysTillExpiry=-7, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-3, quality=4)\n"
                + "Item(daysTillExpiry=-11, quality=24)\n"
                + "Item(daysTillExpiry=-8, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=2, quality=44)\n"
                + "Item(daysTillExpiry=-3, quality=0)\n"
                + "Item(daysTillExpiry=-8, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-4, quality=2)\n"
                + "Item(daysTillExpiry=-12, quality=26)\n"
                + "Item(daysTillExpiry=-9, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=1, quality=47)\n"
                + "Item(daysTillExpiry=-4, quality=0)\n"
                + "Item(daysTillExpiry=-9, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-5, quality=0)\n"
                + "Item(daysTillExpiry=-13, quality=28)\n"
                + "Item(daysTillExpiry=-10, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=0, quality=50)\n"
                + "Item(daysTillExpiry=-5, quality=0)\n"
                + "Item(daysTillExpiry=-10, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-6, quality=0)\n"
                + "Item(daysTillExpiry=-14, quality=30)\n"
                + "Item(daysTillExpiry=-11, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=0)\n"
                + "Item(daysTillExpiry=-6, quality=0)\n"
                + "Item(daysTillExpiry=-11, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-7, quality=0)\n"
                + "Item(daysTillExpiry=-15, quality=32)\n"
                + "Item(daysTillExpiry=-12, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=-2, quality=0)\n"
                + "Item(daysTillExpiry=-7, quality=0)\n"
                + "Item(daysTillExpiry=-12, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-8, quality=0)\n"
                + "Item(daysTillExpiry=-16, quality=34)\n"
                + "Item(daysTillExpiry=-13, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=-3, quality=0)\n"
                + "Item(daysTillExpiry=-8, quality=0)\n"
                + "Item(daysTillExpiry=-13, quality=0)\n"
                + "\n"
                + "Item(daysTillExpiry=-9, quality=0)\n"
                + "Item(daysTillExpiry=-17, quality=36)\n"
                + "Item(daysTillExpiry=-14, quality=0)\n"
                + "Item(daysTillExpiry=0, quality=80)\n"
                + "Item(daysTillExpiry=-1, quality=80)\n"
                + "Item(daysTillExpiry=-4, quality=0)\n"
                + "Item(daysTillExpiry=-9, quality=0)\n"
                + "Item(daysTillExpiry=-14, quality=0)\n\n", s.toString());
    }
    
}
