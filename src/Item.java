import lombok.*;


/**
 * Created by stephen on 3/31/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public abstract class Item {
    
    public int daysTillExpiry;
    public int quality;
    
    protected abstract void process();
    
    protected void updateDaysTillExpiry() {
        daysTillExpiry--;
    }
    
    protected void updateQuality(int changeInQuality) {
        quality = Math.min(quality + changeInQuality, 50);
        if (quality <= 0) {
            quality = 0;
        }
        
    }
    
    static class LegendaryItem extends Item {
        
        public LegendaryItem(int daysTillExpiry, int quality) {
            super(daysTillExpiry, quality);
        }
        
        @Override
        protected void process() {
            //Legendary items are not affected
        }
    }
    
    
    static class WineItem extends Item {
        
        public WineItem(int daysTillExpiry, int quality) {
            super(daysTillExpiry, quality);
        }
        
        @Override
        protected void process() {
            updateDaysTillExpiry();
            if (daysTillExpiry < 0) {
                updateQuality(2);
            } else {
                updateQuality(1);
            }
        }
    }
    
    
    static class ConcertTickets extends Item {
        
        public ConcertTickets(int daysTillExpiry, int quality) {
            super(daysTillExpiry, quality);
        }
        
        @Override
        protected void process() {
            updateDaysTillExpiry();
            calculateQuality();
        }
        
        private void calculateQuality() {
            if (daysTillExpiry < 0) {
                quality = 0;
            } else if (daysTillExpiry < 5) {
                updateQuality(3);
            } else if (daysTillExpiry < 10) {
                updateQuality(2);
            } else {
                updateQuality(1);
            }
        }
    }
    
    
    static class PerishableItem extends Item {
        
        public PerishableItem(int daysTillExpiry, int quality) {
            super(daysTillExpiry, quality);
        }
        
        @Override
        protected void process() {
            updateDaysTillExpiry();
            updateQuality(-2);
        }
    }
    
    
    static class NormalItem extends Item {
        
        public NormalItem(int daysTillExpiry, int quality) {
            super(daysTillExpiry, quality);
        }
        
        @Override
        protected void process() {
            updateDaysTillExpiry();
            if (daysTillExpiry < 0) {
                updateQuality(-2);
            } else {
                updateQuality(-1);
            }
        }
    }
}
